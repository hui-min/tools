> 该插件库为作者平常经常用到的方法集合，将不定期迭代

> 已加入composer仓库，可通过composer require hamster/tools安装

> com目录/lib目录中每个文件均可独立下载使用

## 常用的基本处理 lib

### 数组 Harray
- sortArray - 根据某字段指定顺序对数组进行排序
- splitArray - 根据某个字段拆分成新数组，并指定保存的字段（支持递归）
- mergeDyadicArray - 合并两个二维数组（根据指定的key值）
- parseParams - 将字符解析成数组
- apartPageData - 把全部数据分页拆开

### 字符串 Hstring
- isJson - 判断是否json格式
- cutStr - 截取文字，超出使用省略号
- randomString - 生成随机字符
- drawImgSrc - 正则图片路径提取
- checkHttpUrl - 检查是否http链接
- utf8ize - 异常utf8格式转换（json_decode报错JSON_ERROR_UTF8时可用）

### 时间 Htime
- checkEqualWeek - 判断是否在同一星期
- timeToSec - 时间转成秒（不带日期）
- secToTime - 秒转时间

### 请求 Hcurl
- curlGet - 简单的curl
- curlRequest - curl请求
- sockRequest - 异步请求
- getRequestIp - 获取请求ip

### 计算 Hcompute
- computeScale - 计算百分比
- computeDivision - 计算除法

### 文件 Hfile
- csvGetLines - 文件读取
- delFileUnderDir - 删除原有的文件目录（支持逐级删除子目录）

### 距离 Hdistance
- getDistance - 坐标距离计算

### 密码 Hpassword89 
- hashEncrypt - 密码加密
- hashAuth - 密码校验
- passwordRegex - 校验密码规则是否正确（长度是否符合，是否包含数组/大写字母/小写字母/符号中的三种）

* * *

## 公共类库 com

### redis缓存 Hredis

```
  $config = [
	'host'      => '127.0.0.1',
	'password'  => null,
	'port'      => 6379,
	'database'  => 0,
  ];
  $model = \hamster\tools\com\Hredis::getObject($config);
```

### mongodb数据库 HmongoDb
```
  $config = [
      'hostname'=>'127.0.0.1',
      'database'=>'admin?authMechanism=SCRAM-SHA-1',
      'username'=>'root',
      'password'=>'root',
      'hostport'=>'27017',
      'db_name'=>'miniapp',
  ];
  $MongoModel = HmongoDb::getObject($config);
  $row = $MongoModel->select('collection', ['id'=>1]);
```

### xss过滤 XssFilter
```
  $html = XssFilter::discuz_remove_xss($html);
```

### 文件上传 Hupload
``` 
  $file_data = $_FILES['file'];
  $Upload = new \hamster\tools\com\Hupload();
  $Upload->fileSave($file_data, 'tmp');
```

### ssdb数据库 
``` 
  $option = [
    'host'       => '127.0.0.1',
    'port'       => 7379, // 官方默认端口8888
    'password'   => '', // 在执行第一条命令的时候才发送，密码是明文传输
    'timeout_ms' => 2000, // 连接超时时间，单位毫秒（在不使用ssdb_php扩展，引用ssdb.php文件时不能为0，否则回提示timeout）
  ];
  $ssdbObject = new \SimpleSSDB($option['host'], $option['port'], $option['timeout_ms']);
  $ssdbObject->auth($option['password']);
```

### Rsa非对称加密
```
  $rsa = new \hamster\tools\com\Rsa(__DIR__ . '\rsa_private_key.pem', __DIR__ . '\rsa_public_key.pem');
  $rsa->publicEncrypt('1234');
```

### Aes
```
  $aes = new \hamster\tools\com\Aes('1234123412341234', '1234123412341234', OPENSSL_ZERO_PADDING);
  var_dump($aes->encrypt('test', 'OFB'));
  var_dump($aes->decrypt('sI7JFA==', 'OFB'));
```

### Des
```
  $des = new \hamster\tools\com\Des('1234123412341234', 'DES-OFB', '12345678', OPENSSL_NO_PADDING);
  var_dump($des->encrypt('test'));
  var_dump($des->decrypt('ZyK/0mq25DY='));
```