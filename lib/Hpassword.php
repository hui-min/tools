<?php

namespace hamster\tools\lib;

// 密码工具
class Hpassword
{
	/**
	 * 密码加密
	 * @param string $password 加密前密码
	 * @return false|string|null 失败返回false，算法无效返回null
	 */
	public static function hashEncrypt($password)
	{
		// PASSWORD_BCRYPT选项salt(盐值)php7之后已被废弃，会自动生成随机验值
		$encrypt_password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 8]);

		return $encrypt_password;
	}

	/**
	 * 密码校验
	 * @param string $password 加密前密码
	 * @param string $encrypt_password 加密后密码
	 * @return bool
	 */
	public static function hashAuth($password, $encrypt_password)
	{
		return password_verify($password, $encrypt_password);
	}

	/**
	 * 校验密码规则是否正确（长度是否符合，是否包含数组/大写字母/小写字母/符号中的三种）
	 * @param string $password 明文密码
	 * @param int $min 最短长度
	 * @param int $max 最长长度
	 * @return bool
	 */
	public static function passwordRegex($password, $min=8, $max=16)
	{
		if (preg_match('/^(?![a-zA-Z]+$)(?![A-Z0-9]+$)(?![A-Z\W_]+$)(?![a-z0-9]+$)(?![a-z\W_]+$)(?![0-9\W_]+$)[a-zA-Z0-9\W_]{'.$min.','.$max.'}$/', $password)) {
			return true;
		} else {
			return false;
		}
	}
}