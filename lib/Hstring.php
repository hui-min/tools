<?php

namespace hamster\tools\lib;

// 字符串工具
class Hstring
{
	/**
	 * 判断是否json格式
	 * @param $string
	 * @return bool
	 */
    public static function isJson($string)
    {
        $data = json_decode($string);
        if (is_object($data) || is_array($data)) {
            return true;
        } else {
            return false;
        }
    }

	/**
	 * 截取文字，超出使用省略号
	 * @param string $str 被处理数据
	 * @param integer $len 截取长度
	 * @param string $suffix 填充后缀
	 * @return string
	 */
    public static function cutStr($str, $len, $suffix = "...")
    {
        if (function_exists('mb_substr')) {
            if (mb_strlen($str, 'utf8') > $len) {
                $str = mb_substr($str, 0, $len) . $suffix;
            }
            return $str;
        } else {
            if (mb_strlen($str, 'utf8') > $len) {
                $str = substr($str, 0, $len) . $suffix;
            }
            return $str;
        }
    }

	/**
	 * 生成随机字符
	 * @param int $length 长度
	 * @param int $type 类型
	 * @return string
	 */
    public static function randomString($length = 8, $type = 1)
    {
        // 密码字符集，可任意添加你需要的字符
        switch ($type) {
            case 1:
                $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
                break;
            case 2:
                $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                break;
            case 3:
                $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_ []{}<>~`+=,.;:/?|';
                break;
            default:
                $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        }
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            // 这里提供两种字符获取方式
            // 第一种是使用 substr 截取$chars中的任意一位字符；
            // 第二种是取字符数组 $chars 的任意元素
            // $string .= substr($chars, mt_rand(0, strlen($chars) – 1), 1);
            $string .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $string;
    }

	/**
	 * 正则图片路径提取
	 * @param string $html html代码 （eg:<img src="http://baidu.com">）
	 * @return mixed （eg:http://baidu.com）
	 */
    public static function drawImgSrc($html)
    {
//    $imgpreg = '/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i';
        $imgpreg = '/<img.*?src=[\"|\']?(.*?)[\"|\']?\s?>/i';
        preg_match_all($imgpreg, $html, $img);
        return $img[1];
    }

	/**
	 * 检查是否http链接
	 * @param string $url 链接地址
	 * @return bool
	 */
	public static function checkHttpUrl($url){
		$preg = "/http[s]?:\/\/[\w.]+[\w\/]*[\w.]*\??[\w=&\+\%]*/is";
		if (preg_match($preg,$url)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 异常utf8格式转换（json_decode报错JSON_ERROR_UTF8时可用）
	 * @param $mixed
	 * @return array|string
	 */
	public static function utf8ize($mixed)
	{
		if (is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = self::utf8ize($value);
			}
		} elseif (is_string($mixed)) {
			return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
		}
		return $mixed;
	}

}