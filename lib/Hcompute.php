<?php

// 计算工具
namespace hamster\tools\lib;

class Hcompute
{
	/**
	 * @desc  计算百分比
	 * @param number $divisor 除数
	 * @param number $dividend 被除数
	 * @param int $mode 小数点后保留位数
	 * @param string $symbol 符号
	 * @return int|string （eg:22.22%）
	 */
	public static function computeScale($divisor, $dividend, $mode = 2, $symbol = '%')
	{
		if ($dividend == 0) {
			if ($divisor == 0) {
				return 0 . $symbol;
			} else {
				return 0;
			}
		} else {
			return round($divisor / $dividend * 100, $mode) . $symbol;
		}
	}

	/**
	 * @desc  计算除法
	 * @param number $divisor 除数
	 * @param number $dividend 被除数
	 * @param int $mode 小数点后保留位数
	 * @param string $symbol 符号
	 * @return string
	 */
	public static function computeDivision($divisor, $dividend, $mode = 2, $symbol = '')
	{
		if ($dividend == 0) {
			return round(0, $mode) . $symbol;
		} else {
			return round($divisor / $dividend, $mode) . $symbol;
		}
	}
}