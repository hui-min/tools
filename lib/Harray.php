<?php

// 数组工具
namespace hamster\tools\lib;

class Harray
{
    /**
     * @desc 根据某字段指定顺序对数组进行排序
     * @param array $data 处理数据
     * @param string $column 查询字段
     * @param array $sortData 排序数据
     * @return array
     */
    public static function sortArray($data, $column, $sortData)
    {
        $temp = [];
        foreach ($data as $val) {
            $temp[$val[$column]] = $val;
        }

        $newData = [];
        foreach ($sortData as $val) {
            if (!empty($temp[$val])) {
                $newData[] = $temp[$val];
            }
        }

        return $newData;
    }

    /**
     * @desc 根据某个字段拆分成新数组，并指定保存的字段（支持递归）
     * @param array $data 处理数据 （eg:[{type: 1,uin: 1001},{type: 1,uin: 1002},{type: 2,uin: 2001},{type: 2,uin: 2002}]）
     * @param string|int $key 拆分字段（eg:type）
     * @param string|int $column 获取字段（eg:uin）
     * @param string|int $recursion 递归字段
     * @param array $newData 递归前保存的数据
     * @return array （eg:{1: [1001,1002],2: [2001,2002]}）
     */
    public static function splitArray($data, $key, $column, $recursion = null, &$newData = [])
    {
        foreach ($data as $val) {
            $newData[$val[$key]][] = $val[$column];

            if (isset($recursion) && !empty($val[$recursion])) {
                self::splitArray($val[$recursion], $key, $column, $recursion, $newData);
            }
        }
        return $newData;
    }

    /**
     * @desc 合并两个二维数组（根据指定的key值）
     * @param array $array1 第一个二维数组（主）（eg:[{"id":1001,"type":1},{"id":1002,"type":2}]）
     * @param array $array2 第二个二维数组（副）（eg:[{"id":1001,"test":3},{"id":1002,"test":4}]）
     * @param string|int $key1 第一个数组的key
     * @param string|int $key2 第二个数组的key
     * @param bool $iskey 是否使用指定key值作为新的数组的key
     * @return array （eg:{"1001":{"id":1001,"type":1,"test":3},"1002":{"id":1002,"type":2,"test":4}}）
     */
    public static function mergeDyadicArray($array1, $array2, $key1, $key2, $iskey = true)
    {
        $newArray = [];
        foreach ($array2 as $val2) {
            $newArray[$val2[$key2]] = $val2;
        }

        $data = [];
        foreach ($array1 as $val1) {
            if (isset($newArray[$val1[$key1]])) {
                if ($iskey) {
                    $data[$val1[$key1]] = array_merge($val1, $newArray[$val1[$key1]]);
                } else {
                    $data[] = array_merge($val1, $newArray[$val1[$key1]]);
                }
            } else {
                if ($iskey) {
                    $data[$val1[$key1]] = $val1;
                } else {
                    $data[] = $val1;
                }
            }
        }

        return $data;
    }


	/**
	 * 将字符解析成数组
	 * @param string $str （eg:name=Bill&age=60）
	 * @return array （eg:['name'=>'Bill', 'age'=>60]）
	 */
    public static function parseParams($str)
    {
        $arrParams = [];
        parse_str(html_entity_decode(urldecode($str)), $arrParams);
        return $arrParams;
    }

	/**
	 * 把全部数据分页拆开
	 * @param array $data 列表数据
	 * @param integer $page 页码
	 * @param integer $pageSize 分页
	 * @return array
	 */
    public static function apartPageData($data, $page, $pageSize)
	{
		$offset = ($page * $pageSize) - $pageSize;
		$page_data = array_splice($data, $offset, $pageSize);

		return $page_data;
	}


}