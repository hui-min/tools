<?php

namespace hamster\tools\lib;

// 时间换算工具
class Htime
{

	/**
	 * 判断是否在同一星期
	 * @param string|integer $time 日期/时间/时间戳
	 * @return bool
	 */
    public static function checkEqualWeek($time)
    {
        $start = mktime(0, 0, 0, date("m"), date("d") - date("w") + 1, date("Y"));
        $end   = mktime(23, 59, 59, date("m"), date("d") - date("w") + 7, date("Y"));

        if (!is_numeric($time)) $time = strtotime($time);
        if ($start <= $time && $time <= $end) {
            return true;
        } else {
            return false;
        }
    }

	/**
	 * 时间转成秒（不带日期）
	 * @param string $times （eg:01:20:00）
	 * @return int
	 */
    public static function timeToSec($times)
    {
        $timeArr = explode(':', $times);
        $sec     = $timeArr[0] * 3600 + $timeArr[1] * 60 + $timeArr[2];

        return $sec;
    }

    /**
     * @desc 秒转时间
     * @param int $sec 秒数
     * @param int $type 返回格式类型（需要的在switch里面增加类型）
     * @return string|array
     */
    public static function secToTime($sec, $type = 0)
    {
        $day       = '0';
        $hour      = '00';
        $totalHour = '00';
        $minute    = '00';
        $second    = '00';
        if ($sec > 0) {
            $day       = floor($sec / 86400);
            $hour      = floor(($sec - 86400 * $day) / 3600);
            $totalHour = floor($sec / 3600);
            $minute    = floor(($sec - 3600 * $totalHour) / 60);
            $second    = floor((($sec - 3600 * $totalHour) - 60 * $minute) % 60);
//        $result = $hour.':'.$minute.':'.$second;
        }
        $result['day']       = $day;
        $result['hour']      = $hour;
        $result['totalHour'] = $totalHour;
        $result['minute']    = $minute;
        $result['second']    = $second;

		switch ($type) {
			case 1: // 30:30:30
				$time = sprintf("%02d", $totalHour) . ':' . sprintf("%02d", $minute) . ':' . sprintf("%02d", $second);
				break;
			case 2: // 30时30分30秒
				$time = sprintf("%02d", $totalHour) . '时' . sprintf("%02d", $minute) . '分' . sprintf("%02d", $second) . '秒';
				break;
			case 11: // 1 06:30:30
				$time = $day . ' ' . sprintf("%02d", $hour) . ':' . sprintf("%02d", $minute) . ':' . sprintf("%02d", $second);
				break;
			case 12: // 1天06时30分30秒
				$time = $day . '天' . sprintf("%02d", $hour) . '时' . sprintf("%02d", $minute) . '分' . sprintf("%02d", $second) . '秒';
				break;
			case 21: // 2020-02-02 06:30:30
				$time = date('Y-m-d', $day*86400) . ' ' . sprintf("%02d", $hour) . ':' . sprintf("%02d", $minute) . ':' . sprintf("%02d", $second);
				break;
			case 22: // 2020年02月02日 06时30分30秒
				$time = date('Y年m月d日', $day*86400) . ' ' . sprintf("%02d", $hour) . '时' . sprintf("%02d", $minute) . '分' . sprintf("%02d", $second) . '秒';
				break;
			default: // 数组格式返回
				$time = $result;
		}
		return $time;
    }

}