<?php

namespace hamster\tools\com;

use MongoDB\BSON\ObjectID;
use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Command;
use MongoDB\Driver\Exception\CommandException;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;

// php7 mongo类
class HmongoDb
{
	public static $mongo_object = null;
	public static $mongo_config = [
		'hostname'  => '127.0.0.1',
		'username'  =>'',
		'password'  => '',
		'hostport'  => 27017,
		'database'  => 0,
		'db_name'   => 'hamster',
	];


    // $config ['hostname', 'password', 'username', 'hostport', 'database', 'db_name', '']
    public function __construct($config)
    {
        $host         = 'mongodb://' . ($config['username'] ? "{$config['username']}" : '') . ($config['password'] ? ":" . urlencode($config['password']) . "@" : '') . $config['hostname'] . ($config['hostport'] ? ":{$config['hostport']}" : '') . '/' . ($config['database'] ? "{$config['database']}" : '');
        $this->config = $config;
        $this->db     = new Manager($host);
    }

	public static function getObject($config=[])
	{
		if (self::$mongo_object) {
			return self::$mongo_object;
		}

		if (empty($config)) {
			$config = self::$mongo_config;
		}

		return new self($config);
	}

	/**
	 * 清空集合数据
	 * @param string $collection 集合数据
	 * @return \MongoDB\Driver\WriteResult
	 */
    public function nearRemove($collection)
    {
        $bulk       = new BulkWrite();

        $bulk->delete([]);
        $result = $this->db->executeBulkWrite("{$this->config['db_name']}.{$collection}", $bulk);

        return $result;
    }

    /**
     * 插入数据
     * @param string $collection 集合名称
     * @param array $data 插入数据
     * @param string $existColumn 判断是否存在的字段
     * @return bool|ObjectID
     */
    public function insert($collection, $data, $existColumn = null)
    {
        $bulk = new BulkWrite();

        if ($existColumn) {

            $bulk->update(
                [$existColumn => $data[$existColumn]],
                ['$set' => $data],
                ['multi' => false, 'upsert' => true]// multi是否更新所有数据（false只更新第一条），upsert不存在数据时是否创建记录
            );
            $result = $this->db->executeBulkWrite("{$this->config['db_name']}.{$collection}", $bulk);

        } else {
            if (!isset($data['_id'])) {
                $id          = new ObjectID();
                $data['_id'] = $id;
            }
            $bulk->insert($data);
            $result = $this->db->executeBulkWrite("{$this->config['db_name']}.{$collection}", $bulk);
        }

        // 成功的话，返回一个MongoDB\Driver\WriteResult实例。
        if (get_class($result) == 'MongoDB\Driver\WriteResult') {
            if (!empty($id)) {
                return $id;
            }
            return true;
        } else {
            return false;
        }
    }

	/**
	 * 批量插入数据
	 * @param string $collection 集合名称
	 * @param array $data 插入数据
	 * @param null $existColumn
	 * @return bool|ObjectID
	 */
    public function insertAll($collection, $data, $existColumn = null)
    {
        $bulk = new BulkWrite();

        if ($existColumn) {

            foreach ($data as $value) {
                $bulk->update(
                    [$existColumn => $value[$existColumn]],
                    ['$set' => $value],
                    ['multi' => false, 'upsert' => true]// multi是否更新所有数据（false只更新第一条），upsert不存在数据时是否创建记录
                );
            }
            $result = $this->db->executeBulkWrite("{$this->config['db_name']}.{$collection}", $bulk);

        } else {

            foreach ($data as $value) {
                if (!isset($data['_id'])) {
                    $id          = new ObjectID();
                    $data['_id'] = $id;
                }
                $bulk->insert($value);
            }
            $result = $this->db->executeBulkWrite("{$this->config['db_name']}.{$collection}", $bulk);

        }

        // 成功的话，返回一个MongoDB\Driver\WriteResult实例。
        if (get_class($result) == 'MongoDB\Driver\WriteResult') {
            if (!empty($id)) {
                return $id;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * 添加索引
     * @param string $collection 集合名
     * @param string $indexName 索引名
     * @param array $indexKeys 索引字段数组
     * @return bool
	 * @throws \MongoDB\Driver\Exception\Exception
     */
    public function addIndex($collection, $indexName, $indexKeys)
    {
        $cmd = [
            'createIndexes' => $collection, //集合名
            'indexes'       => [
                [
                    'name'       => $indexName, //索引名
                    'key'        => $indexKeys, //索引字段数组 $indexKeys  = array( 'name' => 1, 'age' => 1 );
                    'unique'     => false,
                    'background' => true,
                ],
            ],
        ];

        $command  = new Command($cmd);
        $result   = $this->db->executeCommand($this->config['db_name'], $command);
        $response = current($result->toArray());

        if ($response->ok == 1) {
            return true;
        } else {
            return false;
        }
    }

	/**
	 * 更新数据
	 * @param string $collection 集合名称
	 * @param array $filter 筛选条件
	 * @param array $data 插入数据
	 * @return bool
	 */
    public function update($collection, $filter, $data)
    {
        $bulk = new BulkWrite();

        $bulk->update(
            $filter,
            ['$set' => $data],
            ['multi' => true, 'upsert' => false]
        );
        $result = $this->db->executeBulkWrite("{$this->config['db_name']}.{$collection}", $bulk);

        // 成功的话，返回一个MongoDB\Driver\WriteResult实例。
        if (get_class($result) == 'MongoDB\Driver\WriteResult') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除数据
     * @param string $collection 集合名称
     * @param array $filter 查询条件
     * @return bool
     */
    public function delete($collection, $filter)
    {
        $bulk = new BulkWrite();

        $bulk->delete($filter, ['multi' => true, 'upsert' => false]);
        $result = $this->db->executeBulkWrite("{$this->config['db_name']}.{$collection}", $bulk);

        // 成功的话，返回一个MongoDB\Driver\WriteResult实例。
        if (get_class($result) == 'MongoDB\Driver\WriteResult') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 查询数据
     * @param string $collection 集合名称
     * @param array $filter 查询条件
     * @param array $options
     * @return array
	 * @throws \MongoDB\Driver\Exception\Exception
     */
    public function select($collection, $filter, $options = [])
    {
        $query  = new Query($filter, $options);
        $cursor = $this->db->executeQuery("{$this->config['db_name']}.{$collection}", $query);

        $list = [];
        foreach ($cursor as $val) {
            $list[] = (array) $val;
        }
        return $list;
    }

    /**
     * 统计数据
     * @param string $collection 集合名称
     * @param array $filter
     * @return int
	 * @throws \MongoDB\Driver\Exception\Exception
     */
    public function count($collection, $filter = [])
    {
        $command = new Command(['count' => $collection, 'query' => $filter]);
        $result  = $this->db->executeCommand($this->config['db_name'], $command);
        $res     = $result->toArray();

        $num = 0;
        if ($res) {
            $num = $res[0]->n;
        }
        return $num;
    }

    /**
     * 删除集合
     * @param string $collection 集合名称
     * @return bool
	 * @throws \MongoDB\Driver\Exception\Exception
     */
    public function deleteCollection($collection)
    {
        try {
            $result = $this->db->executeCommand($this->config['db_name'], new Command(["drop" => $collection]));

            // 成功的话，返回一个MongoDB\Driver\WriteResult实例。
            if (get_class($result) == 'MongoDB\Driver\Cursor') {
                return true;
            } else {
                return false;
            }
        } catch (CommandException $e) {
            return false;
        }
    }

	/**
	 * 聚合查询
	 * @param string $collection 集合名称
	 * @param array $filter 筛选条件
	 * @param array $group 分组
	 * @param array $sort 排序
	 * @param int $limit 查询数量
	 * @return mixed
	 * @throws \MongoDB\Driver\Exception\Exception
	 */
    public function aggregate($collection, $filter=[], $group=[], $sort=[], $limit=0)
    {
        $cmd = [
            'aggregate' => $collection,
            'pipeline' => [
//                ['$match' => $filter],
//                ['$group' => $group],
//                ['$sort' => $sort],
//                ['$limit' => $limit],
            ]
        ];

        if ($filter) {
            $cmd['pipeline'][] = ['$match' => $filter];
        }
        if ($group) {
            $cmd['pipeline'][] = ['$group' => $group];
        }
        if ($sort) {
            $cmd['pipeline'][] = ['$sort' => $sort];
        }
        if ($limit) {
            $cmd['pipeline'][] = ['$limit' => $limit];
        }

        $command  = new Command($cmd);
        $cursor   = $this->db->executeCommand($this->config['db_name'], $command)->toArray();


        $list = $cursor[0]->result;

        return $list;
    }
}
