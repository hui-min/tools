<?php

namespace hamster\tools\com;

// 原生redis类
class Hredis
{
	public static $redis_object = null;
	public static $redis_config = [
		'host'      => '127.0.0.1',
		'password'  => '',
		'port'      => 6379,
		'database'  => 0,
	];

	public static function getObject($config=[])
	{
		if (self::$redis_object) {
			return self::$redis_object;
		}

		if (empty($config)) {
			$config = self::$redis_config;
		}

		$redis = new \Redis();
		$redis->connect($config['host'], $config['port']);
		if ($config['password']) {
			$redis->auth($config['password']);
		}
		$redis->select($config['database']);

		self::$redis_object = $redis;
		return self::$redis_object;
	}

}